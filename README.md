# Transaction Lifecycle in Ethereum PoS

This doccument describes transaction lifecycle in Ethereum PoS from creation to finality in the blockchain

## Transaction Creation

Firstly transaction should be created by specifying fields such as:
- `from` – the address of the sender, that will be signing the transaction. This will be an externally-owned account as contract accounts cannot send transactions.
- `recipient` – the receiving address (if an externally-owned account, the transaction will transfer value. If a contract account, the transaction will execute the contract code)
- `nonce` - a sequentially incrementing counter which indicates the transaction number from the account
- `value` – amount of ETH to transfer from sender to recipient (denominated in WEI, where 1ETH equals 1e+18wei)
- `data` – optional field to include arbitrary data that could be used by smart contracts
- `gasLimit` – the maximum amount of gas units that can be consumed by the transaction. The EVM specifies the units of gas required by each computational step
- `maxPriorityFeePerGas` - the maximum price of the consumed gas to be included as a tip to the validator
- `maxFeePerGas` - the maximum fee per unit of gas willing to be paid for the transaction (inclusive of `baseFeePerGas` and `maxPriorityFeePerGas`)

## Signing the Transaction

Once the transaction has been created, its hash needs to be signed by the sender using their private key. This ensures that only the sender can authorize the transfer of funds or arbitrary action in smart contract.

## Broadcasting the Transaction

After the transaction has been signed, it needs to be broadcasted to the Ethereum network. This is done by submitting the transaction to a node using RPC API, which will then relay it to other nodes in the network.

Transactions could also be broadcasted by node itself without using RPC API in case the node has additional business logic, generating it or taking it from external sources.

## Transaction Propagation

Once the transaction has been broadcast to the network, it will be propagated to other nodes using peer-to-peer communication. After receiving nodes will verify the transaction and add it to their mempool, which is a list of unconfirmed transactions waiting to be included in a block.

Transaction propagation is completely optional and not required by consensus. Some nodes could use this as an opportunity to include specific transaction in their own blocks without showing it to the other nodes. Node chooses what transaction to propagate and what to include into newly created block. This property is used in projects such as Flashbots Auction to allow users to include their transactions in blocks without risk of someone frontrunning it.

## Adding Transaction in Newly Created Block

One of the nodes on the network is the block proposer for the current slot, having previously been selected pseudo-randomly using RANDAO. This node is responsible for building and broadcasting the next block to be added to the Ethereum blockchain and updating the global state. The node is made up of three parts: an execution client, a consensus client and a validator client. The execution client bundles transactions from the local mempool into an "execution payload" and executes them locally to generate a state change. This information is passed to the consensus client where the execution payload is wrapped as part of a "beacon block" that also contains information about rewards, penalties, slashings, attestations etc that enable the network to agree on the sequence of blocks at the head of the chain. While creating block validator nodes are free to include transactions they want which are not necessary from mempool populated during transaction propagation from other nodes.

Other nodes receive the new beacon block on the consensus layer gossip network. They pass it to their execution client where the transactions are re-executed locally to ensure the proposed state change is valid. The validator client then attests that the block is valid and is the logical next block in their view of the chain (meaning it builds on the chain with the greatest weight of attestations as defined in the fork choice rules). The block is added to the local database in each node that attests to it.

Sometimes Etehreum nodes have different view of the chain (usually due to latency of message propagation between nodes). This could lead to chain fork in which there could be several blocks with the same height. Nodes use fork choice rule which is designed to ensure that the network reaches a consensus on the state of the blockchain and the canonical chain is secure and resistant to attacks. Ethereum PoS uses Gasper protocol, which is a combination of the GHOST and Casper FFG ideas.

Time in proof-of-stake Ethereum is divided into slots (12 seconds) and epochs (32 slots). One validator is randomly selected to be a block proposer in every slot. This validator is responsible for creating a new block and sending it out to other nodes on the network. Also in every slot, a committee of validators is randomly chosen, whose votes are used to determine the validity of the block being proposed.

## Transaction Finality in Block

Finality is a property of certain blocks that means they cannot be reverted unless there has been a critical consensus failure and an attacker has destroyed at least 1/3 of the total staked ether. Finalized blocks can be thought of as information the blockchain is certain about. A block must pass through a two-step upgrade procedure for a block to be finalized:
- Two-thirds of the total staked ether must have voted in favor of that block's inclusion in the canonical chain. This condition upgrades the block to "justified". Justified blocks are unlikely to be reverted, but they can be under certain conditions.
- When another block is justified on top of a justified block, it is upgraded to "finalized". Finalizing a block is a commitment to include the block in the canonical chain. It cannot be reverted unless an attacker destroys millions of ether (billions of $USD).

These block upgrades do not happen in every slot. Instead, only epoch-boundary blocks can be justified and finalized. These blocks are known as "checkpoints". Upgrading considers pairs of checkpoints. A "supermajority link" must exist between two successive checkpoints (i.e. two-thirds of the total staked ether voting that checkpoint B is the correct descendant of checkpoint A) to upgrade the less recent checkpoint to finalized and the more recent block to justified.

In case block of our transaction is not finalized there could be reorganization using fork choice rule meaning transaction could possibly not end up in cannonical blockchain.
